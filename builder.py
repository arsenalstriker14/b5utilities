#!/usr/bin/env python
import glob, os
import os.path, time
import shutil
import getpass

user = getpass.getuser()


def builddir():
    source_dir = raw_input("Where is your source directory? : ")
    target_dir = raw_input("What is your target directory? : ")
    dir_name = raw_input("Name your new directory: ")
    source_dir = source_dir.replace("\ "," ").rstrip()
    filelist = []
    os.chdir(source_dir)
    for file in glob.glob("*"):
        if file.startswith('.'):
            continue
        filelist.append(file)

    stage_dir = '/Users/' + user + '/Desktop/filemanager/archive/' + target_dir + '/' + dir_name
    stage = os.makedirs(stage_dir)

    for item in filelist:
        prefix = item[:15]
        sub = item[16:19]
        new_dir = stage_dir + "/" + prefix
        sub_dir = stage_dir + "/" + prefix + "/" + sub
        if not os.path.exists(new_dir):
            os.mkdir(new_dir)
        if not os.path.exists(sub_dir):
            os.mkdir(sub_dir)

    for item in filelist:
        prefix = item[:15]
        sub = item[16:19]
        new_dir = stage_dir + "/" + prefix
        sub_dir = stage_dir + "/" + prefix + "/" + sub
        src_file = source_dir + "/" + item

        dst_file = os.path.join(sub_dir, item)
        shutil.copy(src_file, sub_dir)
        
    print("sku folders created and populated")

if __name__ == "__main__":
    builddir()