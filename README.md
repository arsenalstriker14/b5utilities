search 1
---------------------------
the search1 utility allows for quick, targeted searching of the One Image file directory and obviates the necessity of manually trolling through the individual sku and color directories. operators can enter a search term in a number of formats:

4120_14096_2310_015_large_50.jpg
4120 14096 2310 015 AQ.eps
4120_14096_2310
4120-14096-2310
4120 14096 2310
4120140962310

any of the above entries will search One Info and return any file with a 4120 14096 2310 prefix, along with the last modification date (typically the upload date) associated with it. Additionally, typing or pasting a file name into the searchfield, followed by the command 'open', will open that image file in photoshop - providing immediate visual confirmation of the file present in system

search export
----------------------------
offers the same functionality of the search 1 utility for the "large" directory on the Export drive. This functionality is especially useful for the expedited resolution of image name duplication, and file sequence conflicts.


search eyemagic
-----------------------------
the search eyemagic utility allows the operator to search the eymagic (recd from) repository to determine if eyemagic provided an input filename - and if so, when it was sent and which ad it is associated with.

directory builder
----------------------------
the directory builder takes an image folder (like EyeMagic's Print folder) and builds a proper sku directory from it. for each file in the image folder with a Big 5, long sku title prefix, the builder will create a folder titled with the class, vendor style numbers - place a folder with the color code inside of it, and copy the file inside of that. 

extension stripper
------------------------------
the extension stripper prepares web files for automatic sequencing by removing any sequence numbers already attached to the file name.
by stripping these pre-existing numbers, the files may be re-sequenced, using real-time data from the Export drive - providing optimal protection from inadvertent naming collisions.

sequencer
--------------------------------
the sequencer utility matches the prefix (short sku) of each file in the filemanager webfiles directories with the same prefix of files residing on the export drive. it then attaches an appropriate sequence number to the target file based upon what already exists on the Export drive.
The sequencer also replaces "-" with "_", and otherwise formats the targeted filename appropriately for the Export drive. For example, when the sequencer comes accross file 0310-11111-3401-015-xalt.jpg in the filemanager, it will search for the prefix 0310-11111-3401 on Export. Finding files 0310_11111_3401_015_large_02.jpg,  0310_11111_3401_081_large_03.jpg, and  0310_11111_3401_015_large_70.jpg already on export... the sequencer will recognize the 'xalt' on the target file and start in the 70's; it will then notice that the highest '70' existing for the target prefix on export belongs to 0310_11111_3401_015_large_70.jpg, so it will reformat the target file file to read 0310_11111_3401_015_large_71.jpg and move on to the next webfile in the filemanager.


file sync
---------------------------------
the filemanager contains an archive directory in which print files are arranged and held by their respective skus. the filemanager also contains web file directories where the webfiles are formatted and sequenced by version size (large, regular, ms, thumb). once the webfiles are properly sequenced, the sync utility can be used to automatically copy each webfile in the filemanager to it's associated folder in the sku directory; essentially creating complete sku archive folders with a single command.  


file deployment
---------------------------------- 
the Big 5 image management protocol requires image files to be distributed to different servers/directories, in different formats and package hierachies. For instance, webfiles are delivered to their apprpriate Export directories and WebImages on the ADVERTISING and MERCHANDISING servers, wheras print files are delivered to sku folders to One Image, color code folders in Vendor Images and directly to the IQ HotFolder. In some cases, new holding directories must be created; in others, not.
The deploy utility greatly simplifies this process by automating it - building the rules for each server and file type into code that is automatically executed upon each file passed to it.
Additionally, the deploy utility provides feedback at each step of the process and because it is a 'non-destructive' process (with the exception of the explicit 'rename' and 'remove' utilities - none of the utilities will ever overwrite, or otherwise modify a file pre-existing on any server/drive) it gives helpful notification if it can't upload a file to, say, One Image or Vendor Images beacuse a file of that name already exisits. So in addition to deploying files where they need to go, the deploy utility is also very helpful in the file management process as well.


file remove
----------------------------------


file rename
----------------------------------