#!/usr/bin/env python
import os
import shutil
import getpass

target_folder = raw_input("What is your target directory? : ")
user = getpass.getuser()



def distribute(size):

    # target_folder = raw_input("What is your target directory? : ")
    prefix = '/Users/' + user + '/Desktop/' + 'filemanager/archive/' + target_folder + '/'

    root_src_dir = '/Users/' + user + '/Desktop/filemanager/' + size

    for src_dir, dirs, files in os.walk(root_src_dir):
        for file_ in files:
            if file_.startswith('.'):
                continue
            folder = file_[:15].replace('_', ' ').replace('-', ' ') + '/'
            sub = file_[16:19] + '/'

            root_dst_dir = prefix + folder + sub
            dst_dir = src_dir.replace(root_src_dir, root_dst_dir)

            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                continue
            shutil.copy2(src_file, dst_dir)
    print( size + " files moved")


if __name__ == "__main__":
    distribute('large')
    distribute('regular')
    distribute('ms')
    distribute('thumb')
    
