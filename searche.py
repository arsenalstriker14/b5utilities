#!/usr/bin/env python
import glob, os
import os.path, time
import shutil
import getpass
import subprocess

os.chdir("/Volumes/EXPORT/Color/large")

root_src_dir = '/Volumes/EXPORT/Color/large'

searchstring =''

while searchstring != 'quit':
	searchstring = raw_input("type a searchstring (enter 'quit' to exit) : ")
	if searchstring == 'quit':
		break
	# elif searchstring.endswith('.jpg') or searchstring.endswith('open'):
	elif searchstring.endswith('open'):

		sku = searchstring[:4] + ' ' +  searchstring[5:10] + ' ' + searchstring[11:15]
		file_list = searchstring.split(' ')
		file_ = file_list[0]
		target = root_src_dir + '/' + file_
		os.chdir(root_src_dir)
		file_to_open = file_
		try:
			subprocess.Popen(['open', '-a', '/Applications/Adobe Photoshop CC/Adobe Photoshop CC.app', file_to_open], cwd=root_src_dir)
		except:
			for file in glob.glob(searchstring + "*"):
				date = time.ctime(os.path.getmtime(root_src_dir+"/"+file))
				print
				print(file, date)
				print

	elif searchstring.find(' ') == -1:
		searchstring = searchstring.replace('_', '').replace('-', '')
		searchstring = searchstring[:4] + '_' +  searchstring[4:9] + '_' + searchstring[9:13]
		print('searching for prefix: ' + str(searchstring))
		for file in glob.glob(searchstring + "*"):
			date = time.ctime(os.path.getmtime(root_src_dir+"/"+file))
			print
			print(file, date)
			print

	else:
		searchstring = searchstring.replace(' ', '')
		searchstring = searchstring[:4] + '_' +  searchstring[4:9] + '_' + searchstring[9:13]
		for file in glob.glob(searchstring + "*"):
			date = time.ctime(os.path.getmtime(root_src_dir+"/"+file))
			print
			print(file, date)
			print