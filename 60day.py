#!/usr/bin/env python
import os, glob
import os.path, time
import datetime
import shutil

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def clean60():
	root_src_dir = "/Volumes/ADVERTISING/Inserts/Pete/60DayArchive"

	for src_dir, dirs, files in walklevel(root_src_dir, level=0):
		threshold_time = datetime.date.today() - datetime.timedelta(days=60)
		for dir_ in dirs:
			date = os.stat(os.path.join(root_src_dir, dir_)).st_mtime
			date = datetime.datetime.fromtimestamp(date)
			print(threshold_time)
			print(date)
			if date.date() < threshold_time:
				target_dir = os.path.join(root_src_dir, dir_)
				for src_dir, dirs, files in walklevel(target_dir, level=0):
					for subdir in dirs:
						while not os.listdir(os.path.join(target_dir, subdir)):
							shutil.rmtree(subdir)
				try:
					shutil.rmtree(target_dir)
				except:
					try:
					# os.path.join(targeted_dir, new_filename)
                    # os.rename(src_file, dst_file)

						os.rmdir(target_dir)
					except:
						print("can't remove " + dir_ + " programatically. try manual removal.")
						continue

	for src_dir, dirs, files in walklevel(root_src_dir, level=0):
		for dir_ in dirs:
			if not os.listdir(os.path.join(root_src_dir, dir_)):
				try:
					shutil.rmtree(os.path.join(root_src_dir, dir_))
				except:
					try:
						os.rmdir(os.path.join(root_src_dir, dir_))
					except:
						continue
											

if __name__ == "__main__":
    clean60()
