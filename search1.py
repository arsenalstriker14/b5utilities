#!/usr/bin/env python
import glob, os
import os.path, time
import shutil
import getpass
import subprocess

user = getpass.getuser()

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def searchOneImage():

    searchstring = ''
    while searchstring != "quit":
        root_src_dir = '/Volumes/ADVERTISING/Inserts Archive/ONE IMAGE INFO CENTER'
        searchstring = raw_input("type a searchstring (enter 'quit' to exit): ")

        if searchstring.find(' ') == -1:
            searchstring = searchstring[:4] + ' ' +  searchstring[4:9] + ' ' + searchstring[9:13]
    
        sku = searchstring[:4] + ' ' +  searchstring[5:10] + ' ' + searchstring[11:15]
        parent = ''
        file_array = []
        parent_dir_code = searchstring[:4]


        for src_dir, dirs, files in walklevel(root_src_dir, level=0):
            for dir_ in dirs:
                if dir_.startswith(parent_dir_code):
                    parent = dir_
                    break

        if searchstring.endswith('open'):
            target = root_src_dir + '/' + parent + '/' + sku

            try:
                os.chdir(target)
            except:
                print('the requested directory does not exist')
                break
            file_to_open = searchstring[:-5]
            
            subprocess.Popen(['open', '-a', '/Applications/Adobe Photoshop CC/Adobe Photoshop CC.app', file_to_open], cwd=target)


        elif searchstring != ('quit'):
            target = root_src_dir + '/' + parent + '/' + sku
            
            try:
                os.chdir(target)
            except:
                print('the requested directory does not exist')
                continue
            for file in glob.glob(sku + "*"):
                date = time.ctime(os.path.getmtime(target+"/"+file))
                print
                print(file, date)
                print

if __name__ == "__main__":
    searchOneImage()