#!/usr/bin/env python
import glob, os
import shutil
import getpass


def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]


target = raw_input("What is the file to rename? : ")
new_filename = raw_input("What is the new filename? : ")

def renameWebfiles(size, target, new_filename):

    target_dirs = ['/Volumes/EXPORT/Color/large', '/Volumes/EXPORT/Color/regular', '/Volumes/EXPORT/Color/thumb']
    # target_dirs = ['/Users/krcnyc/Desktop/filemanager/large', '/Users/krcnyc/Desktop/filemanager/regular', '/Users/krcnyc/Desktop/filemanager/thumb']


    if target.find('large') != -1:
        target = target.replace('large', size)
    elif target.find('regular') != -1:
        target = target.replace('regular', size)
    elif target.find('thumb') != -1:
        target = target.replace('thumb', size)
    elif target.find('ms') != -1:
        target = target.replace('ms', size)
    else:
        pass
    
    if new_filename.find('large') != -1:
        new_filename = new_filename.replace('large', size)
    elif new_filename.find('regular') != -1:
        new_filename = new_filename.replace('regular', size)
    elif new_filename.find('thumb') != -1:
        new_filename = new_filename.replace('thumb', size)
    elif new_filename.find('ms') != -1:
        new_filename = new_filename.replace('ms', size)
    else:
        pass

    for dir_ in target_dirs:
        os.chdir(dir_)
        for file in glob.glob(target):
            src_file = os.path.join(dir_, file)
            dst_file = os.path.join(dir_, new_filename)
            os.rename(src_file, dst_file)
            print(target + " >> " + new_filename)

def renameMS(size, target, new_filename):

    root_src_dir = '/Volumes/MARKETING/RLA/Content Creation/Carousel - Website/z_microsite_images'
    # root_src_dir = '/Users/krcnyc/Desktop/z-micro'
    
    prefix = target[0]

    if target.find('large') != -1:
        target = target.replace('large', size)
    elif target.find('regular') != -1:
        target = target.replace('regular', size)
    elif target.find('thumb') != -1:
        target = target.replace('thumb', size)
    elif target.find('ms') != -1:
        target = target.replace('ms', size)
    else:
        pass

    if new_filename.find('large') != -1:
        new_filename = new_filename.replace('large', size)
    elif new_filename.find('regular') != -1:
        new_filename = new_filename.replace('regular', size)
    elif new_filename.find('thumb') != -1:
        new_filename = new_filename.replace('thumb', size)
    elif new_filename.find('ms') != -1:
        new_filename = new_filename.replace('ms', size)
    else:
        pass

    for src_dir, dirs, files in walklevel(root_src_dir, level=0):
        for dir_ in dirs:
            if dir_[0] == prefix:
                os.chdir(os.path.join(root_src_dir, dir_))
                targeted_dir = os.path.join(root_src_dir, dir_)
                for file in glob.glob(target):
                    src_file = os.path.join(targeted_dir, file)
                    dst_file = os.path.join(targeted_dir, new_filename)
                    os.rename(src_file, dst_file)
                    print(target + " >> " + new_filename)
            

def renameOneImage():
    root_dst_dir = '/Volumes/ADVERTISING/Inserts Archive/ONE IMAGE INFO CENTER'

    query = raw_input("Rename print files on One Image? (y or n) : " )

    if query[0] == "y":
        target = " "
        while target != "done":
            target = raw_input("Type the filename to rename OR type 'done' : " )
            new_filename = raw_input("What is the new filename? : ")

            for src_dir, dirs, files in walklevel(root_dst_dir, level=0):
                # dst_dir = src_dir.replace(root_src_dir, root_dst_dir)
                # if not os.path.exists(dst_dir):
                #     os.mkdir(dst_dir)

                product_code = target[:4]
                sku = target[:4] + ' ' +  target[5:10] + ' ' + target[11:15]
                sub = target[16:19] 

                # os.chdir(os.path.join(src_subdir)
                # for file in glob.glob(target):
                #   os.remove(file)

                for dir_ in dirs:
                    if dir_[:4] == product_code:
                        file_dir = root_dst_dir + "/" + dir_ + "/" + sku + "/"

                        os.chdir(file_dir)
                        for file in glob.glob(target):
                            src_file = os.path.join(file_dir, file)
                            dst_file = os.path.join(file_dir, new_filename)
                            os.rename(src_file, dst_file)
                            print(target + " >> " + new_filename)



if __name__ == "__main__":
    # renameWebfiles('large', target, new_filename)
    # renameWebfiles('regular', target, new_filename)
    # renameWebfiles('thumb', target, new_filename)
    # renameMS('ms', target, new_filename)
    renameOneImage()
