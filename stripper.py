#!/usr/bin/env python
import glob, os
import shutil
import getpass


def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

src_dir = raw_input("Where are your source files? : ")
src_dir = src_dir.replace("\ "," ").rstrip()

def stripVersion(size, src_dir):

    user = getpass.getuser()
    root_src_dir = src_dir + '/' + size
    count = 1

    os.chdir(root_src_dir)
    for file in glob.glob("*"):
        prefix = file[:19]
        try:
            version = int(file[-6:-4])
            if version >= 50 and version <70:
                count += 1
                tag = '-xmod' + str(count)
            elif version >= 70:
                count += 1
                tag = '-xalt' + str(count)
            else:
                tag =''

        except:
            continue
        new_filename = prefix.replace('_', '-') + tag + '.jpg'
        src_file = os.path.join(root_src_dir, file)
        dst_file = os.path.join(root_src_dir, new_filename)
        os.rename(src_file, dst_file)

    print(size + " version numbers removed")

if __name__ == "__main__":
    stripVersion('large', src_dir)
    stripVersion('regular', src_dir)
    stripVersion('ms', src_dir)
    stripVersion('thumb', src_dir)
