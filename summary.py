#!/usr/bin/env python
import glob, os
import shutil
import getpass

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]



def summary():

    # target_folder = raw_input("What is your target directory? : ")
    # prefix = '/Users/' + user + '/Desktop/' + 'filemanager/archive/' + target_folder + '/'

    # root_src_dir = '/Users/' + user + '/Desktop/filemanager/' + size
    user = getpass.getuser()

    week_start = raw_input('What is the start date? :')
    week_end = raw_input('What is the end date? :')
    week = week_start + " - " + week_end

    weekly_skus = 0
    weekly_colors = 0
    target_folder = ''
    sku_list = []
    color_list = []
    folder_list = []
    f1=open('/Users/' + user + '/Desktop/summary.txt', 'w+')

    while target_folder != "done":
        target_folder = raw_input("add a target directory: ")
        target_folder = target_folder.rstrip().replace('\\ ', ' ')
        if target_folder != "done":
            folder_list.append(target_folder)

    for item in folder_list:
        root_src_dir = item

        os.chdir(root_src_dir)
        for file in glob.glob("*"):
            if file.startswith('.'):
                continue
            sku_prefix = file[:15]
            color_prefix = file[:19]

            if sku_prefix not in sku_list:
                sku_list.append(sku_prefix)
            if color_prefix not in color_list:
                color_list.append(color_prefix)

        weekly_skus == len(sku_list)
        weekly_colors == len(color_list)

    print >> f1, "Sku's processed " + week + ":"
    print >> f1, sku_list
    print >> f1, "Color codes processed " + week + ":"
    print >> f1, color_list
    print >> f1, 'Total number of skus processed ' + week + ' : ' + str(len(sku_list))
    print >> f1, 'Total number of colors processed ' + week + ' : ' + str(len(color_list))


if __name__ == "__main__":
    summary()
    