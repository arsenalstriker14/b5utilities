#!/usr/bin/env python

import os, glob
import os.path, time
import shutil
import datetime
import smtplib
from email.mime.text import MIMEText

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def logRecent():

	path = '/Volumes/EXPORT/Color/large/'
	file_arr = os.listdir(path)

	now = time.time()
	# yesterday = datetime.now() - datetime.timedelta(days=2)
	twodays_ago = now - 60*60*24*2

	# textfile = '/Users/krc/b5utils/temp/exportreport.txt'
	# fp = open(textfile, 'w')

	for item in file_arr:
		date = os.stat(os.path.join(path + item)).st_mtime
		# filetime = time.ctime(os.path.getctime(os.path.join(path + item)))
		filetime = time.ctime(os.stat(os.path.join(path + item)).st_mtime)
		if item.startswith('.'):
			continue
		# if date == yesterday:
		if date > twodays_ago and date < now:
			# fp.write(item + ", " + filetime + "\n")
			print(item + ", " + filetime)


	# fp.close()
	# fp = open(textfile, 'rb')
	# # Create a text/plain message
	# msg = MIMEText(fp.read())
	# fp.close()
		
	# msg['Subject'] = "Export Report TEST"
	# msg['From'] = "krc@big5corp.com"
	# msg['To'] = "krc@big5corp.com"

	# # Send the message via our own SMTP server, but don't include the
	# # envelope header.
	# s = smtplib.SMTP('https://riv-mftexh00.big5corp.com/EWS/Exchange.asmx')
	# s.sendmail(me, [you], msg.as_string())
	# s.quit()


if __name__ == "__main__":
	logRecent()


