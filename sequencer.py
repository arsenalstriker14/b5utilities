#!/usr/bin/env python
import glob, os
import shutil
import getpass

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

# export_dir = '/Volumes/EXPORT/Color/large'
path = '/Volumes/EXPORT/Color/large'
file_arr = os.listdir(path)

# filelist = open('/Users/krcnyc/Desktop/export_latest.txt', 'r')
# for line in filelist:
#     line = line.rstrip()
#     if line.startswith('.'):
#         continue
#     file_arr.append(line)

# for src_dir, dirs, files in walklevel(export_dir, level=0):
#     for file_ in files:
#         file_arr.append(file_)

def renameIt(size, file_arr):

    user = getpass.getuser()
    root_src_dir = '/Users/' + user + '/Desktop/filemanager/' + size
    newfile = ''

    highest_normal = 01
    highest_model = 49
    highest_alt = 69
    
    holding_array = []
    alt_holder = []
    mod_holder = []
    export_arr = []

    for src_dir, dirs, files in walklevel(root_src_dir, level=0):
        for file_ in sorted(files):
            raw_prefix = file_[:15]
            sequence_prefix = file_[:15]
            prefix = file_[:15].replace('-', '_')
            sub = file_[16:19]
            if file_.startswith('.'):
                continue      
            print(prefix)
            # for src_dir, dirs, files in walklevel(root_src_dir, level=0):
            #     for file_ in files:
            #         if file_.find('mod') == -1 and file_.find('alt') == -1:
            #             holding_array.append(file_)
            #         elif file_.find('mod') != -1:
            #             mod_holder.append(file_)
            #         else:
            #             alt_holder.append(file_)

            os.chdir(root_src_dir)
            for file in sorted(glob.glob(raw_prefix + "*")):
                if file.find('xmod') == -1 and file.find('xalt') == -1:
                    holding_array.append(file)
                elif file.find('xmod') != -1:
                    mod_holder.append(file)
                else:
                    alt_holder.append(file)

            print(mod_holder)
            print(alt_holder)
            # os.chdir(root_src_dir)
            # for file in sorted(glob.glob(raw_prefix + "*")):
            file_arr = sorted(file_arr)
            for item in file_arr:
                if item.startswith(prefix):
                    export_arr.append(item)

            # print(export_arr)
            export_arr = sorted(export_arr)
            for item in export_arr:

                try:
                    version = int(item[-6:-4])
                    print(version)
                except:
                    continue

                if version >= highest_normal and version < highest_model:
                    highest_normal = version
                elif version >= highest_model and version < highest_alt:
                    highest_model = version
                elif version >= highest_alt:
                    highest_alt = version


            # print(prefix, highest_normal)
            # print(prefix, highest_model)
            # print(prefix, highest_alt)
            def getIndex(arr):
                try:
                    return(arr.index(file_))
                except:
                    return(20) 

            if file_.find('xalt') != -1:
                newnum = highest_alt + 1 + getIndex(alt_holder)
                newfile = raw_prefix + "-" + sub + "-" + size + "-" + str(newnum).zfill(2) + 'xalt.jpg'
            elif file_.find('xmod') != -1:
                newnum = highest_model + 1 + getIndex(mod_holder)
                newfile = raw_prefix + "-" + sub + "-" + size + "-" + str(newnum).zfill(2) + 'xmod.jpg'
            else:
                newnum = highest_normal + 1 + getIndex(holding_array)
                newfile = raw_prefix + "-" + sub + "-" + size + "-" + str(newnum).zfill(2) + '.jpg'                

       

            src_file = os.path.join(root_src_dir, file_)
            dst_file = os.path.join(root_src_dir, newfile)
            print(dst_file)
            os.rename(src_file, dst_file)

            holding_array = []
            alt_holder = []
            mod_holder = []
            export_arr = []

            highest_normal = 01
            highest_model = 49
            highest_alt = 69    

    os.chdir(root_src_dir)
    for file in glob.glob("*"):
        new_filename = file.replace('-', '_').replace('xalt*', '.jpg').replace('xmod*', '.jpg').replace('xalt', '').replace('xmod', '')
        src_file = os.path.join(root_src_dir, file)
        dst_file = os.path.join(root_src_dir, new_filename)
        os.rename(src_file, dst_file)
        print(new_filename)


if __name__ == "__main__":
    renameIt('large', file_arr)
    renameIt('regular', file_arr)
    renameIt('ms', file_arr)
    renameIt('thumb', file_arr)
