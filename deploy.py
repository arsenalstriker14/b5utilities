#!/usr/bin/env python
import os
import shutil
import getpass

user = getpass.getuser()
target_folder = raw_input("What is your source directory? : ")
web_images_folder = raw_input("What is WebImages date? : ")
print('----------')

# acceptable sizes are 'large', 'regular', 'ms and 'thumb'
# destination accepts a path as a string argument
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

def deploy(size, destination):
    root_src_dir = '/Users/' + user + '/Desktop/filemanager/' + size
    root_dst_dir = destination

    for src_dir, dirs, files in os.walk(root_src_dir):
        dst_dir = src_dir.replace(root_src_dir, root_dst_dir)
        
        for file_ in files:
            if file_.startswith('.'):
                continue
            src_file = os.path.join(src_dir, file_)
            dst_file = os.path.join(dst_dir, file_)
            if os.path.exists(dst_file):
                print(dst_file + " is a duplicate... not deployed")
                continue
            shutil.copy(src_file, dst_dir)
    print(size + ' jpgs copied to export')



######################

def deployMS():

    root_src_dir = '/Users/' + user + '/Desktop/filemanager/ms'
    root_dst_dir = '/Volumes/MARKETING/RLA/Content\ Creation/Carousel\ -\ Website/z_microsite_images'
    root_dst_dir = root_dst_dir.replace('\ ',  ' ')
    prefix = ''

    for src_dir, dirs, files in walklevel(root_src_dir, level=0):
        for file_ in files:
            prefix = file_[0]
            if file_.startswith('.'):
              continue
            for src_dir, dirs, files in walklevel(root_dst_dir, level=0):
                for dir_ in dirs:
                  if dir_[0] == prefix:
                    print(dir_)
                    dst_dir = root_dst_dir + '/' + dir_
                    src_file = os.path.join(root_src_dir, file_)
                    dst_file = os.path.join(dst_dir, file_)

                    if os.path.exists(dst_file):
                      print(file_ + " : already there")
                      continue
                    shutil.copyfile(src_file, dst_file)
                    
            
    print('jpgs copied to z-microsites')
    print('----------')
    

def deployWebImages(size, web_images_folder):

    root_src_dir = '/Users/' + user + '/Desktop/filemanager/' + size.lower()
    root_dst_dir = '/Volumes/ADVERTISING/Inserts/ALBERT'
    prefix = ''

    dst_dir = "WebImages" + ' ' + web_images_folder

    
    if not os.path.exists(os.path.join(root_dst_dir, dst_dir)):
        os.chdir(root_dst_dir)
        os.mkdir(dst_dir)
        os.chdir(os.path.join(root_dst_dir, dst_dir))
        os.mkdir('Large')
        os.mkdir('Regular')
        os.mkdir('Thumb')


    for src_dir, dirs, files in os.walk(root_src_dir):
        for file_ in files:
            final_dir = os.path.join(root_dst_dir, dst_dir)  + '/' + size
            src_file = os.path.join(root_src_dir, file_)
            dst_file = os.path.join(final_dir, file_)


            if os.path.exists(dst_file):
                continue
            shutil.copy(src_file, final_dir)
                        
    print(size + ' jpgs copied to WebImages')
    print('----------')



def exportOne(target_folder):

  root_src_dir = '/Users/' + user + '/Desktop/filemanager/archive/' + target_folder
  root_dst_dir = '/Volumes/ADVERTISING/Inserts Archive/ONE IMAGE INFO CENTER'

  for src_dir, dirs, files in os.walk(root_src_dir):
        target = ''
        product_code = ''
        src_file = ''

        if files:
          for file_ in files:
            if file_.startswith('.'):
              continue
            if file_.endswith('.eps'):

              product_code = file_[:4]
              sku = file_[:4] + ' ' +  file_[5:10] + ' ' + file_[11:15]
              sub = file_[16:19] 
              src_file = root_src_dir + "/" + sku + "/" + sub + "/" + file_

              for src_dir, dirs, files in walklevel(root_dst_dir, level=0 ):
                for dir_ in dirs:
                  if dir_[:4] == product_code:
                    target = dir_


              for src_dir, dirs, files in walklevel(root_dst_dir, level=0):
                for dir_ in dirs:
                  if dir_ == target:
                    
              
                    dst_dir = root_dst_dir + "/" + target + "/" + sku
                    home_dir = root_dst_dir + "/" + target
                    end_dir = sku
                    
                    if not os.path.exists(os.path.join(home_dir, end_dir)):
                      os.chdir(home_dir)
                      os.mkdir(end_dir)
                      print(end_dir + " created")

                      dst_file = os.path.join(dst_dir, file_)
                      if os.path.exists(dst_file):
                        continue
                      shutil.copy(src_file, dst_dir)
                    else:

                      dst_file = os.path.join(dst_dir, file_)
                      
                      if os.path.exists(dst_file):
                        print(file_ + " : File already in One Image")
                        continue
                      shutil.copy(src_file, dst_dir)
                      print(file_ + " : uploaded to One Image")
                      

  print('eps files deployed to One Media')
  print('----------')


def exportHotIQ(target_folder):

  root_src_dir = '/Users/' + user + '/Desktop/filemanager/archive/' + target_folder
  root_dst_dir = '//Volumes/ADVERTISING/BG5_Hotfolders/IQHotPhoto'

  for src_dir, dirs, files in os.walk(root_src_dir):
    
        target = ''
        product_code = ''
        src_file = ''

        if files:
          for file_ in files:
            if file_.startswith('.'):
              continue
            if file_.endswith('.eps'):

              #product_code = file_[:4]
              sku = file_[:4] + ' ' +  file_[5:10] + ' ' + file_[11:15]
              sub = file_[16:19] 
              src_file = root_src_dir + "/" + sku + "/" + sub + "/" + file_

              dst_dir = root_dst_dir
              dst_file = os.path.join(dst_dir, file_)
              
              if os.path.exists(dst_file):
                print(file_ + " : File already in HotIQ")
                continue
              shutil.copy(src_file, dst_dir)
              print(file_ + " : uploaded to HotIQ")
                      

  print('eps files uploaded to HotIQ')
  print('----------')


def export60day(target_folder):

  root_src_dir = '/Users/' + user + '/Desktop/filemanager/archive/' + target_folder
  root_dst_dir = '/Volumes/ADVERTISING/Inserts/PETE/60DayArchive'



  for src_dir, dirs, files in os.walk(root_src_dir):
        target = ''
        product_code = ''
        src_file = ''

        if files:
          for file_ in files:
            if file_.startswith('.'):
              continue
            if any( [file_.endswith('.eps'), file_.endswith('.psd')] ):

              product_code = file_[:15]
              sku = file_[:4] + ' ' +  file_[5:10] + ' ' + file_[11:15]
              sub = file_[16:19] 
              src_file = root_src_dir + "/" + sku + "/" + sub + "/" + file_

              home_dir = root_dst_dir + "/" + product_code
              end_dir = sub
              dst_dir = home_dir + "/" + end_dir

              try:
                os.chdir(home_dir)
              except:
                os.chdir(root_dst_dir)
                os.mkdir(product_code)
                os.chdir(home_dir)
              
              if not os.path.exists(os.path.join(home_dir, end_dir)):
                os.mkdir(end_dir)

                dst_file = os.path.join(dst_dir, file_)
                if os.path.exists(dst_file):
                  print(file_ + " : File already in 60 Day")
                  continue
                shutil.copy(src_file, dst_dir)
                print(file_ + " : uploaded to temporary archive")
              else:

                dst_file = os.path.join(dst_dir, file_)
                
                if os.path.exists(dst_file):
                  print(file_ + " : File already in 60 Day")
                  continue
                shutil.copy(src_file, dst_dir)
                print(file_ + " : uploaded to temporary archive")
                      

  print('eps files deployed to 60 Day Archive')
  print('----------')


if __name__ == "__main__":
    deploy('large', '/Volumes/EXPORT/Color/large')
    deploy('regular', '/Volumes/EXPORT/Color/regular')
    deploy('thumb', '/Volumes/EXPORT/Color/thumb')
    deployMS()
    deployWebImages('Large', web_images_folder)
    deployWebImages('Regular', web_images_folder)
    deployWebImages('Thumb', web_images_folder)
    exportOne(target_folder)
    exportHotIQ(target_folder)
    export60day(target_folder)
    

