#!/usr/bin/env python
import fnmatch
import os
import os.path, time
import re

excludes = ['Web'] # for dirs and files

# transform glob patterns to regular expressions
# includes = r'|'.join([fnmatch.translate(x) for x in includes])
# excludes = r'|'.join([fnmatch.translate(x) for x in excludes]) or r'$.'

searchstring = raw_input("What file are you looking for? : ")

for dirpath, dirnames, filenames in os.walk('/Volumes/ADVERTISING/Inserts/ALBERT/EYE MAGIC'):

    dirnames[:] = [dirname
        for dirname in dirnames
        if all([dirname.startswith(string) is False
            for string in excludes])
        is True]
    

    for fname in filenames:
        if fname.startswith('.'):
            continue
        if fname == searchstring:
            date = time.ctime(os.path.getmtime(dirpath+"/"+fname))
            print
            print(fname)
            print(dirpath)
            print(date)
            print